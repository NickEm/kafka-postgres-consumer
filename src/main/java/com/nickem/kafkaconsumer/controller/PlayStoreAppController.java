package com.nickem.kafkaconsumer.controller;

import com.nickem.entity.PlayStoreApp;
import com.nickem.kafkaconsumer.dao.PlayStoreAppJdbcDao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
public class PlayStoreAppController {

    private final PlayStoreAppJdbcDao playStoreAppJdbcDao;

    @Autowired
    public PlayStoreAppController(final PlayStoreAppJdbcDao playStoreAppJdbcDao) {
        this.playStoreAppJdbcDao = playStoreAppJdbcDao;
    }

    @PostMapping("/play-store-app")
    public ResponseEntity createPlayStoreAppMigration() {

        PlayStoreApp application = new PlayStoreApp();
        application.setApp("Photo Editor & Candy Camera & Grid & ScrapBook");
        application.setCategory("ART_AND_DESIGN");
        application.setRating(Double.valueOf("4.1"));
        application.setReviews(159);
        application.setSize("19M");
        application.setInstalls("10,000+");
        application.setType("Free");
        application.setPrice(Double.valueOf("0"));
        application.setContentRating("Everyone");
        application.setGenres("Art & Design");
        application.setLastUpdated(LocalDate.parse("January 7, 2018", DateTimeFormatter.ofPattern("MMMM d, yyyy")));
        application.setCurrentVer("1.0.0");
        application.setAndroidVer("4.0.3 and up");

        playStoreAppJdbcDao.insert(application);

        return ResponseEntity.ok("Item is set");
    }
}
