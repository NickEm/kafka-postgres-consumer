package com.nickem.kafkaconsumer.service;

import com.nickem.entity.PlayStoreApp;
import com.nickem.kafkaconsumer.dao.KafkaProcessingJdbcDao;
import com.nickem.kafkaconsumer.dao.PlayStoreAppJdbcDao;
import com.nickem.kafkaconsumer.entity.KafkaProcessing;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.util.Pair;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.time.Instant;
import java.util.Date;

@Service
public class PlayStoreAppConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlayStoreAppConsumer.class);
    private static final String CONSUMER_1 = "play-store-consumer-1";
    private static final String CONSUMER_2 = "play-store-consumer-2";
    private static final String CONSUMER_3 = "play-store-consumer-3";

    private final PlayStoreAppJdbcDao playStoreAppJdbcDao;
    private final KafkaProcessingJdbcDao kafkaProcessingJdbcDao;
    private final String groupId;

    public PlayStoreAppConsumer(final PlayStoreAppJdbcDao playStoreAppJdbcDao,
                                final KafkaProcessingJdbcDao kafkaProcessingJdbcDao,
                                @Value("${spring.kafka.consumer.group-id}") final String groupId) {
        this.playStoreAppJdbcDao = playStoreAppJdbcDao;
        this.kafkaProcessingJdbcDao = kafkaProcessingJdbcDao;
        this.groupId = groupId;
    }

    @KafkaListener(clientIdPrefix = CONSUMER_1, topics = "${kafka-topics.play-store-app.name}")
    public void listenForPlayStoreApp(@Payload final PlayStoreApp app,
                                      @Header(KafkaHeaders.CONSUMER) final KafkaConsumer<String, String> consumer,
                                      @Header(KafkaHeaders.RECEIVED_PARTITION_ID) final Integer partitionId,
                                      @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) final String messageKey,
                                      @Header("kafka_producedTimestamp") final Long timestamp,
                                      @Header(KafkaHeaders.RECEIVED_TIMESTAMP) final Long receivedTimestamp,
                                      @Header(KafkaHeaders.OFFSET) final Long offset) {
        LOGGER.info("Consumer {} received the message: {}", CONSUMER_1, app, timestamp);

        handleAppMessage(app, partitionId, messageKey, timestamp, receivedTimestamp, offset, getConsumerClientId(consumer));
    }

    @KafkaListener(clientIdPrefix = CONSUMER_2, topics = "${kafka-topics.play-store-app.name}")
    public void consumer2(@Payload final PlayStoreApp app,
                          @Header(KafkaHeaders.CONSUMER) final KafkaConsumer consumer,
                          @Header(KafkaHeaders.RECEIVED_PARTITION_ID) final Integer partitionId,
                          @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) final String messageKey,
                          @Header("kafka_producedTimestamp") final Long timestamp,
                          @Header(KafkaHeaders.RECEIVED_TIMESTAMP) final Long receivedTimestamp,
                          @Header(KafkaHeaders.OFFSET) final Long offset) {
        LOGGER.info("Consumer {} received the message: {} {}", CONSUMER_2, app, timestamp);

        handleAppMessage(app, partitionId, messageKey, timestamp, receivedTimestamp, offset, getConsumerClientId(consumer));
    }

    @KafkaListener(clientIdPrefix = CONSUMER_3, topics = "${kafka-topics.play-store-app.name}")
    public void consumer3(@Payload final PlayStoreApp app,
                          @Header(KafkaHeaders.CONSUMER) final KafkaConsumer<String, String> consumer,
                          @Header(KafkaHeaders.RECEIVED_PARTITION_ID) final Integer partitionId,
                          @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) final String messageKey,
                          @Header("kafka_producedTimestamp") final Long timestamp,
                          @Header(KafkaHeaders.RECEIVED_TIMESTAMP) final Long receivedTimestamp,
                          @Header(KafkaHeaders.OFFSET) final Long offset) {
        LOGGER.info("Consumer {} received the message: {}", CONSUMER_2, app, timestamp);

        handleAppMessage(app, partitionId, messageKey, timestamp, receivedTimestamp, offset, getConsumerClientId(consumer));
    }

    private void handleAppMessage(final PlayStoreApp app, final Integer partitionId, final String messageKey,
                                  final Long producedTimestamp, final Long receivedTimestamp, final Long offset,
                                  final String clientId) {
        final Pair<Long, PlayStoreApp> idToApp = playStoreAppJdbcDao.insert(app);
        final KafkaProcessing processing =
                new KafkaProcessing(null, clientId, partitionId, messageKey,
                                    Date.from(Instant.ofEpochMilli(Instant.now().toEpochMilli())),
                                    new Date(receivedTimestamp), new Date(),
                                    groupId, offset, app.toString(),
                                    "PlayStoreApp", idToApp.getFirst());
        kafkaProcessingJdbcDao.insert(processing);
    }

    private String getConsumerClientId(final KafkaConsumer consumer) {
        final Field field;
        try {
            field = consumer.getClass().getDeclaredField("clientId");
            field.setAccessible(true);
            return (String) field.get(consumer);
        } catch (NoSuchFieldException | IllegalAccessException e ) {
            LOGGER.warn("Can not get consumer Id.");
        }

        return "undefined";
    }

}
