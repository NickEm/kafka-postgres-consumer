package com.nickem.kafkaconsumer.dao;

import com.nickem.entity.PlayStoreApp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PlayStoreAppJdbcDao {

    private static final String RECORD_ID_COLUMN_KEY = "record_id";

    private static final String APP_COLUMN_KEY = "app";
    private static final String CATEGORY_COLUMN_KEY = "category";
    private static final String RATING_COLUMN_KEY = "rating";
    private static final String REVIEWS_COLUMN_KEY = "reviews";
    private static final String SIZE_COLUMN_KEY = "size";
    private static final String INSTALLS_COLUMN_KEY = "installs";
    private static final String TYPE_COLUMN_KEY = "type";
    private static final String PRICE_COLUMN_KEY = "price";
    private static final String CONTENT_RATING_COLUMN_KEY = "content_rating";
    private static final String GENRES_COLUMN_KEY = "genres";
    private static final String LAST_UPDATED_COLUMN_KEY = "last_updated";
    private static final String CURRENT_VER_COLUMN_KEY = "current_ver";
    private static final String ANDROID_VER_COLUMN_KEY = "android_ver";

    private static final List<String> FIELD_LIST = Arrays.asList(
            APP_COLUMN_KEY,
            CATEGORY_COLUMN_KEY,
            RATING_COLUMN_KEY,
            REVIEWS_COLUMN_KEY,
            SIZE_COLUMN_KEY,
            INSTALLS_COLUMN_KEY,
            TYPE_COLUMN_KEY,
            PRICE_COLUMN_KEY,
            CONTENT_RATING_COLUMN_KEY,
            GENRES_COLUMN_KEY,
            LAST_UPDATED_COLUMN_KEY,
            CURRENT_VER_COLUMN_KEY,
            ANDROID_VER_COLUMN_KEY
    );

    private static final String INSERT_SQL = "INSERT INTO play_store_app(" +
                                             FIELD_LIST.stream().collect(Collectors.joining(", "))
                                             + ") values(:" +
                                             FIELD_LIST.stream().collect(Collectors.joining(",:"))
                                             + ");";

    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    public PlayStoreAppJdbcDao(final NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    public Pair<Long, PlayStoreApp> insert(final PlayStoreApp application) {
        final KeyHolder holder = new GeneratedKeyHolder();
        final SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue(APP_COLUMN_KEY, application.getApp())
                .addValue(CATEGORY_COLUMN_KEY, application.getCategory())
                .addValue(RATING_COLUMN_KEY, application.getRating())
                .addValue(REVIEWS_COLUMN_KEY, application.getReviews())
                .addValue(SIZE_COLUMN_KEY, application.getSize())
                .addValue(INSTALLS_COLUMN_KEY, application.getInstalls())
                .addValue(TYPE_COLUMN_KEY, application.getType())
                .addValue(PRICE_COLUMN_KEY, application.getPrice())
                .addValue(CONTENT_RATING_COLUMN_KEY, application.getContentRating())
                .addValue(GENRES_COLUMN_KEY, application.getGenres())
                .addValue(LAST_UPDATED_COLUMN_KEY, application.getLastUpdated())
                .addValue(CURRENT_VER_COLUMN_KEY, application.getCurrentVer())
                .addValue(ANDROID_VER_COLUMN_KEY, application.getAndroidVer());
        namedJdbcTemplate.update(INSERT_SQL, parameters, holder, new String [] { RECORD_ID_COLUMN_KEY });

        return Pair.of(holder.getKey().longValue(), application);
    }
}
