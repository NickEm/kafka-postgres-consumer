package com.nickem.kafkaconsumer.dao;

import com.nickem.kafkaconsumer.entity.KafkaProcessing;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.util.Pair;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class KafkaProcessingJdbcDao {

    private static final String RECORD_ID_COLUMN_KEY = "record_id";

    private static final String MESSAGE_ID_COLUMN_KEY = "message_key";

    private static final String CONSUMER_ID_COLUMN_KEY = "consumer_id";
    private static final String PARTITION_ID_COLUMN_KEY = "partition_id";
    private static final String GROUP_ID_COLUMN_KEY = "group_id";
    private static final String GROUP_OFFSET_COLUMN_KEY = "group_offset";
    private static final String PRODUCED_DATE_COLUMN_KEY = "produced_date";
    private static final String RECEIVED_DATE_COLUMN_KEY = "received_date";
    private static final String PROCESSED_DATE_COLUMN_KEY = "processed_date";
    private static final String PAYLOAD_COLUMN_KEY = "payload";
    private static final String PAYLOAD_TYPE_COLUMN_KEY = "payload_type";
    private static final String PAYLOAD_REFERENCE_COLUMN_KEY = "payload_reference";

    private static final List<String> FIELD_LIST = Arrays.asList(
            MESSAGE_ID_COLUMN_KEY,
            PARTITION_ID_COLUMN_KEY,
            CONSUMER_ID_COLUMN_KEY,
            GROUP_ID_COLUMN_KEY,
            GROUP_OFFSET_COLUMN_KEY,
            PRODUCED_DATE_COLUMN_KEY,
            RECEIVED_DATE_COLUMN_KEY,
            PROCESSED_DATE_COLUMN_KEY,
            PAYLOAD_COLUMN_KEY,
            PAYLOAD_TYPE_COLUMN_KEY,
            PAYLOAD_REFERENCE_COLUMN_KEY
    );

    private static final String INSERT_SQL = "INSERT INTO kafka_processing(" +
                                             FIELD_LIST.stream().collect(Collectors.joining(", "))
                                             + ") values(:" +
                                             FIELD_LIST.stream().collect(Collectors.joining(",:"))
                                             + ");";

    private final NamedParameterJdbcTemplate namedJdbcTemplate;

    @Autowired
    public KafkaProcessingJdbcDao(final NamedParameterJdbcTemplate namedJdbcTemplate) {
        this.namedJdbcTemplate = namedJdbcTemplate;
    }

    public Pair<Long, KafkaProcessing> insert(final KafkaProcessing processing) {
        final KeyHolder holder = new GeneratedKeyHolder();
        final SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue(MESSAGE_ID_COLUMN_KEY, processing.getMessageKey())
                .addValue(PARTITION_ID_COLUMN_KEY, processing.getPartitionId())
                .addValue(CONSUMER_ID_COLUMN_KEY, processing.getConsumerId())
                .addValue(GROUP_ID_COLUMN_KEY, processing.getGroupId())
                .addValue(GROUP_OFFSET_COLUMN_KEY, processing.getGroupOffset())
                .addValue(PRODUCED_DATE_COLUMN_KEY, processing.getProducedDate())
                .addValue(RECEIVED_DATE_COLUMN_KEY, processing.getReceivedDate())
                .addValue(PROCESSED_DATE_COLUMN_KEY, processing.getProcessedDate())
                .addValue(PAYLOAD_COLUMN_KEY, processing.getPayload())
                .addValue(PAYLOAD_TYPE_COLUMN_KEY, processing.getPayloadType())
                .addValue(PAYLOAD_REFERENCE_COLUMN_KEY, processing.getPayloadReference());
        namedJdbcTemplate.update(INSERT_SQL, parameters, holder, new String [] { RECORD_ID_COLUMN_KEY });

        return Pair.of(holder.getKey().longValue(), processing);
    }

}
