package com.nickem.kafkaconsumer.entity;

import java.util.Date;

import lombok.Data;

@Data
public class KafkaProcessing {

    private final Long recordId;

    private final String consumerId;
    private final Integer partitionId;
    private final String messageKey;
    private final Date producedDate;
    private final Date receivedDate;
    private final Date processedDate;
    private final String groupId;
    private final Long groupOffset;
    private final String payload;
    private final String payloadType;
    private final Long payloadReference;

}
