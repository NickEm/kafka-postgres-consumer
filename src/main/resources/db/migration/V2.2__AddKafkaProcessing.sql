/*! SET storage_engine=INNODB */;

drop table if exists kafka_processing;
create table kafka_processing (
  record_id serial
, message_key varchar(64) null
, partition_id int null
, consumer_id varchar(128) null
, group_id varchar(64) null
, group_offset bigint null
, produced_date timestamp without time zone null
, received_date timestamp without time zone null
, processed_date timestamp without time zone null
, payload text null
, payload_type varchar(256) null
, payload_reference bigint null
, primary key(record_id)
);