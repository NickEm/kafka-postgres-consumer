Kafka Postgres Consumer

Installation

1. To build new docker image execute:
 
		./gradlew clean build && docker build -t kafka-postgres-consumer .
2. To run it execute:
	
		docker run --rm --name kafka-consumer -p 8091:8091 --network kafka-local-network -e SERVER_PORT=8091 kafka-postgres-consumer
